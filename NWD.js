function dzielniki(x){
  // function returns divisors of x 
  var dziel=[];
  var counter=0;
  for(var i=1; i <= x; i++){
    //console.log(x, i, x%i);
    if(x%i === 0){
      dziel[counter]=i;
      //console.log(counter);
      counter++;
    }
  }
  return dziel;
}

function NWD(x, y){
  // function compares divisors of x and y and selects one greatest, common to both
  dx=dzielniki(x);
  dy=dzielniki(y);
  for(var i = dx.length - 1; i >= 0; i--){
    //console.log(dx[i]);
    
    for(var c= dy.length - 1; c >= 0; c--){
      //console.log(dx[i], dy[c]);
      if(dx[i] == dy[c]){
        //console.log("NWD = ", dy[c]);
        return dy[c];
      }
    }
  }
  
  console.log("dx ", dx, dx.length);
  console.log("dy ", dy, dy.length);
}
function NWW(x, y){
  //console.log("NWW = ", x * y / NWD(x, y));
  return x * y / NWD(x, y);
}

var l1 = 42;
var l2 = 56;

console.log("NWD liczb", l1, "i", l2, "to", NWD(l1, l2));
console.log("NWW liczb", l1, "i", l2, "to", NWW(l1, l2));


function czynniki(x){
  var czyn=[];
  var counter=0;
  for(var i=2; i <= x; i++){
    
    if(x%i === 0){
      czyn[counter]=i;
      counter++;
      x = x / i;
      i = 1;
      
    }
  }
  return czyn;
}
var x = 468;
console.log("Czynniki pierwsze liczby ", x, " to: ", czynniki(x));
